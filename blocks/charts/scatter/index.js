import edit from "../edit";
import metadata from "./block.json";

const __ = Drupal.t;

const { category, attributes, supports } = metadata;

const icon = (
	<svg
		xmlns="http://www.w3.org/2000/svg"
		viewBox="0 0 100 100"
		width="250"
		height="250"
	>
		<title id="pie-chart-title">{__("Scatter Chart")}</title>
		<g transform="translate(10,90)" stroke="#000000">
			<line x1="0" y1="0" x2="80" y2="0" stroke-width="1" />
			<line x1="0" y1="0" x2="0" y2="-80" stroke-width="1" />
			<circle cx="20" cy="-20" r="3" fill="#000000" />
			<circle cx="40" cy="-30" r="3" fill="#000000" />
			<circle cx="60" cy="-10" r="3" fill="#000000" />
			<circle cx="70" cy="-50" r="3" fill="#000000" />
			<circle cx="30" cy="-60" r="3" fill="#000000" />
		</g>
	</svg>
);

const settings = {
	...metadata,
	title: __("Scatter Chart"),
	description: __(
		"Display your data in a Scatter Chart. Ideal for a single dataset.",
	),
	icon,
	category,
	attributes,
	supports,
	edit,
	save: () => null,
};

export { settings as scatter };
