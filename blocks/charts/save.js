const Save = ({ attributes }) => {
	const {
		labels,
		title,
		datasets,
		chartBgColor,
		textColor,
		barAxes,
		chartID,
		chartType,
	} = attributes ?? {};

	const dataLabels = JSON.stringify(labels || []);
	const dataDatasets = JSON.stringify(datasets || []);
	const dataTextColor = textColor || "#000000";
	const dataTitle = title || "";
	const dataBarAxes = barAxes || "x";
	const dataType = chartType || "bar";

	return (
		<div className="chart-block-container">
			<canvas
				id={chartID}
				data-labels={dataLabels}
				data-datasets={dataDatasets}
				data-textcolor={dataTextColor}
				data-title={dataTitle}
				data-baraaxes={dataBarAxes}
				data-type={dataType}
				data-chartbgcolor={chartBgColor}
			/>
		</div>
	);
};

export default Save;
