import { InspectorControls } from "@wordpress/block-editor";
import {
	Button,
	ColorPalette,
	FormFileUpload,
	PanelBody,
	PanelRow,
	TextControl,
	TextareaControl,
	ToggleControl,
} from "@wordpress/components";
import { useEffect, useState } from "@wordpress/element";
import Papa from "papaparse";
import { chartColors } from "./chart-config";

const __ = Drupal.t;

const Inspector = ({ attributes, setAttributes }) => {
	if (!attributes) return null;

	const {
		title,
		internal_log,
		datasets,
		chartType,
		textColor,
		sameColor,
		datasetColors: savedDatasetColors,
	} = attributes ?? {};

	const [datasetColors, setDatasetColors] = useState(
		savedDatasetColors ||
			datasets.map((_, index) => chartColors[index % chartColors.length]),
	);

	useEffect(() => {
		setAttributes({ datasetColors });
	}, [datasetColors, setAttributes]);

	useEffect(() => {
		if (datasets && datasets.length > 0) {
			const updatedDatasets = datasets.map((dataset, index) => {
				let borderColor;
				let backgroundColor;

				if (sameColor) {
					borderColor = datasetColors[0];
					backgroundColor =
						chartType === "line" ? "transparent" : datasetColors[0];
				} else {
					borderColor = datasetColors[index];
					backgroundColor =
						chartType === "line" ? "transparent" : datasetColors[index];
				}

				return { ...dataset, borderColor, backgroundColor };
			});

			setAttributes({ datasets: updatedDatasets });
		}
	}, [chartType, datasetColors, sameColor, setAttributes]);

	const handleCSVupload = (e) => {
		const CSVreader = new FileReader();
		CSVreader.onload = () => {
			DataParser(CSVreader.result);
		};
		CSVreader.readAsText(e.target.files[0]);
	};

	const DataParser = (csvData) => {
		Papa.parse(csvData, {
			complete: (results) => {
				const rows = results.data;
				let newDatasets = [];
				const newDatasetColors = [];
				const headerRow = Object.keys(rows[0]);

				const labels = rows.map((row) => row[headerRow[0]]);
				newDatasets = rows.map((row, index) => {
					const color = chartColors[index % chartColors.length];
					newDatasetColors.push(color);
					return {
						label: labels[index],
						data: headerRow.slice(1).map((header) => Number(row[header])),
						borderColor: color,
						backgroundColor: color,
					};
				});

				setAttributes({
					labels: rows.length === 1 ? [headerRow[0]] : headerRow.slice(1),
					datasets: newDatasets,
					datasetColors: newDatasetColors,
				});

				if (chartType === "line") {
					const labels = rows.map((row) => row[headerRow[0]]);
					const data = rows.map((row) => Number(row[headerRow[1]]));
					const color = chartColors[0 % chartColors.length];
					const newDataset = {
						label: headerRow[1],
						data: data,
						borderColor: color,
						backgroundColor: "transparent",
					};

					if (headerRow.length === 2) {
						// Handling single-set CSV for line chart with exactly two columns
						setAttributes({
							labels: labels,
							datasets: [newDataset],
						});
					} else {
						// Handling multi-set CSV for line chart
						const labels = headerRow.slice(1);
						const newDatasets = rows.map((row, index) => {
							const color = chartColors[index % chartColors.length];
							return {
								label: row[headerRow[0]],
								data: labels.map((label) => Number(row[label])),
								borderColor: color,
								backgroundColor: "transparent",
							};
						});

						setAttributes({
							labels: labels,
							datasets: newDatasets,
						});
					}
				}
			},
			header: true,
			skipEmptyLines: true,
		});
	};

	const onDatasetColorChange = (index, newColor) => {
		const newColors = [...datasetColors];
		newColors[index] = newColor;
		setDatasetColors(newColors);
	};

	const onTextColorChange = (newColor) => {
		setAttributes({ textColor: newColor });
	};

	return (
		<InspectorControls>
			<PanelBody title={__("Chart Settings")} initialOpen={true}>
				<PanelRow>
					<TextControl
						label={__("Chart Title")}
						value={title}
						onChange={(newTitle) => setAttributes({ title: newTitle })}
					/>
				</PanelRow>
				<PanelRow>
					<TextareaControl
						label={__("Internal log (optional)")}
						placeholder={__("Add any internal notes needed for the chart here")}
						help={__("For internal note taking only. Not visible to users.")}
						value={internal_log}
						onChange={(newInternalLog) =>
							setAttributes({ internal_log: newInternalLog })
						}
					/>
				</PanelRow>

				<FormFileUpload
					accept=".csv"
					onChange={handleCSVupload}
					render={({ openFileDialog }) => (
						<div>
							<Button
								style={{ marginBottom: "10px" }}
								isPrimary
								onClick={openFileDialog}
							>
								Upload CSV File
							</Button>
						</div>
					)}
				/>
			</PanelBody>

			<PanelBody title={__("Chart Dataset Colors")} initialOpen={true}>
				<ToggleControl
					label={__("Use same color for all labels")}
					checked={sameColor}
					onChange={(value) => setAttributes({ sameColor: value })}
				/>
				{datasets.map((dataset, index) => (
					<PanelRow key={`dataset-${index}`} className="line-color--item">
						<label className="components-base-control__label line-color--label">
							{dataset.label || `Dataset ${index + 1}`}:{" "}
						</label>
						<ColorPalette
							value={datasetColors[index]}
							onChange={(newColor) => onDatasetColorChange(index, newColor)}
						/>
					</PanelRow>
				))}
			</PanelBody>

			<PanelBody title={__("Text Color Settings")} initialOpen={true}>
				<PanelRow>
					<ColorPalette value={textColor} onChange={onTextColorChange} />
				</PanelRow>
			</PanelBody>
		</InspectorControls>
	);
};

export default Inspector;
