import edit from "../edit";
import metadata from "./block.json";

const __ = Drupal.t;

const { category, attributes, supports } = metadata;

const icon = (
	<svg
		version="1.0"
		xmlns="http://www.w3.org/2000/svg"
		width="250pt"
		height="250pt"
		viewBox="0 0 250 250"
		preserveAspectRatio="xMidYMid meet"
	>
		<g
			transform="translate(0,250) scale(0.1,-0.1)"
			fill="#1E1E1E"
			stroke="none"
		>
			<path d="M120 2090 l0 -250 1135 0 1135 0 0 250 0 250 -1135 0 -1135 0 0 -250z" />
			<path d="M110 1280 l0 -310 755 0 755 0 0 310 0 310 -755 0 -755 0 0 -310z" />
			<path d="M120 460 l0 -290 1035 0 1035 0 0 290 0 290 -1035 0 -1035 0 0 -290z" />
		</g>
	</svg>
);

const settings = {
	...metadata,
	title: __("Horizontal Bar Chart"),
	description: __(
		"Display your data in a Horizontal Bar Chart. Ideal for single and multiple datasets.",
	),
	icon: icon,
	category,
	supports,
	attributes,
	edit,
	save: () => null,
};

export { settings as horizontalBar };
