chartColors = [
	"#ff6384",
	"#36a2eb",
	"#cc65fe",
	"#ffce56",
	"#4bc0c0",
	"#f77825",
	"#9966ff",
	"#ff9f40",
	"#ffcd56",
	"#4db6ac",
];

document.addEventListener("DOMContentLoaded", () => {
	const chartCanvases = document.querySelectorAll(
		".chart-block-container canvas",
	);

	for (const canvas of chartCanvases) {
		const labels = JSON.parse(canvas.getAttribute("data-labels"));
		const datasets = JSON.parse(canvas.getAttribute("data-datasets"));
		const textColor = canvas.getAttribute("data-textcolor");
		const title = canvas.getAttribute("data-title");
		const axes = canvas.getAttribute("data-baraaxes");
		const chartType = canvas.getAttribute("data-type");

		const scales =
			chartType === "bar" ||
			chartType === "line" ||
			chartType === "radar" ||
			chartType === "scatter"
				? {
						x: {
							ticks: {
								color: textColor,
							},
						},
						y: {
							ticks: {
								color: textColor,
							},
						},
				  }
				: {};

		for (const [index, dataset] of datasets.entries()) {
			if (chartType === "line" || chartType === "horizontal-line") {
				// For other chart types, assign default colors if not set
				if (!dataset.borderColor && !dataset.backgroundColor) {
					const defaultColor = chartColors[index % chartColors.length];
					dataset.borderColor = defaultColor;
					dataset.backgroundColor =
						chartType === "line" ? "transparent" : defaultColor;
				}
			} else {
				// For pie or doughnut charts, use the provided background colors without modification
				dataset.backgroundColor =
					dataset.backgroundColor ||
					labels.map(
						(_, labelIndex) => chartColors[labelIndex % chartColors.length],
					);
			}
		}

		new Chart(canvas, {
			type: chartType || "bar",
			data: {
				labels: labels,
				datasets: datasets.map((dataset, index) => ({
					...dataset,
					backgroundColor:
						chartType !== "line"
							? dataset.backgroundColor ||
							  labels.map(
									(_, labelIndex) =>
										chartColors[labelIndex % chartColors.length],
							  )
							: "transparent",
					borderColor:
						chartType === "line"
							? dataset.borderColor || chartColors[index % chartColors.length]
							: undefined,
				})),
			},
			options: {
				indexAxis: axes,
				maintainAspectRatio:
					chartType === "pie" ||
					chartType === "doughnut" ||
					chartType === "polarArea" ||
					chartType === "radar"
						? true
						: false,
				plugins: {
					legend: {
						labels: {
							color: textColor,
						},
						display: true,
					},
					title: {
						display: true,
						text: title,
						color: textColor,
					},
				},
				scales: scales,
			},
		});
	}
});
