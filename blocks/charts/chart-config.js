export const chartColors = [
	"#ff6384",
	"#36a2eb",
	"#cc65fe",
	"#ffce56",
	"#4bc0c0",
	"#f77825",
	"#9966ff",
	"#ff9f40",
	"#ffcd56",
	"#4db6ac",
];
