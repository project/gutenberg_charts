import { useBlockProps } from "@wordpress/block-editor";
import { useEffect, useRef, useState } from "@wordpress/element";
import Chart from "chart.js/auto";
import { v4 as uuidv4 } from "uuid";
import Inspector from "./Inspector";
import { chartColors } from "./chart-config";

const Edit = ({ attributes, setAttributes }) => {
	if (!attributes) return null;

	const {
		labels,
		title,
		datasets,
		chartBgColor,
		textColor,
		barAxes,
		chartID,
		chartType,
	} = attributes ?? {};

	const canvasRef = useRef(null);
	const [chartObj, setChartObj] = useState(null);

	useEffect(() => {
		if (!chartID) {
			const newChartID = `chart-${uuidv4()}`;
			setAttributes({ chartID: newChartID });
		}
	}, [chartID, setAttributes]);

	useEffect(() => {
		const ctx = canvasRef.current.getContext("2d");
		if (chartObj) {
			chartObj.destroy();
		}

		const scales =
			chartType === "bar" ||
			chartType === "line" ||
			chartType === "radar" ||
			chartType === "scatter"
				? {
						x: {
							ticks: {
								color: textColor,
							},
						},
						y: {
							ticks: {
								color: textColor,
							},
						},
				  }
				: {};

		const newChart = new Chart(ctx, {
			type: chartType,
			data: {
				labels: labels,
				datasets: datasets.map((dataset, index) => ({
					...dataset,
					backgroundColor:
						chartType !== "line"
							? dataset.backgroundColor ||
							  labels.map(
									(_, labelIndex) =>
										chartColors[labelIndex % chartColors.length],
							  )
							: "transparent",
					borderColor:
						chartType === "line"
							? dataset.borderColor || chartColors[index % chartColors.length]
							: undefined,
				})),
			},
			options: {
				indexAxis: barAxes || "x",
				responsive: true,
				plugins: {
					legend: {
						labels: {
							color: textColor,
						},
						display: true,
					},
					title: {
						display: true,
						text: title,
						color: textColor,
					},
				},
				scales: scales,
			},
		});

		setChartObj(newChart);
		return () => newChart.destroy();
	}, [textColor, labels, datasets, title, barAxes, chartType, chartBgColor]);

	return (
		<div {...useBlockProps()}>
			<Inspector attributes={attributes} setAttributes={setAttributes} />
			<canvas ref={canvasRef} id={chartID} width="400" height="400" />
		</div>
	);
};

export default Edit;
