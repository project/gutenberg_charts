import { bar as barSettings } from "./bar";
import { doughnut as doughnutSettings } from "./doughnut";
import { horizontalBar as horizontalBarSettings } from "./horizontal-bar";
import { horizontalLine as horizontalLineSettings } from "./horizontal-line";
import { line as lineSettings } from "./line";
import { pie as pieSettings } from "./pie";
import { polarArea as polarAreaSettings } from "./polar-area";
import { radar as radarSettings } from "./radar";
import { scatter as scatterSettings } from "./scatter";

import Edit from "./edit";
import Save from "./save";

export {
	barSettings,
	pieSettings,
	doughnutSettings,
	lineSettings,
	horizontalBarSettings,
	horizontalLineSettings,
	scatterSettings,
	radarSettings,
	polarAreaSettings,
	Edit,
	Save,
};
