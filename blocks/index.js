import { registerBlockType } from "@wordpress/blocks";
import { getCategories, setCategories } from "@wordpress/blocks";

import {
	Edit,
	Save,
	barSettings,
	doughnutSettings,
	horizontalBarSettings,
	horizontalLineSettings,
	lineSettings,
	pieSettings,
	polarAreaSettings,
	radarSettings,
	scatterSettings,
} from "./charts";

const chartCategory = {
	slug: "charts",
	title: "Charts",
};

// Register the new categories
const newCategories = [chartCategory];

for (const category of newCategories) {
	if (!getCategories().find((cat) => cat.slug === category.slug)) {
		setCategories([category, ...getCategories()]);
	}
}

const registerChartBlock = (settings) => {
	if (settings) {
		registerBlockType(settings.name, {
			...settings,
			category: chartCategory.slug,
			edit: Edit,
			save: Save,
		});
	}
};

registerChartBlock(barSettings);
registerChartBlock(pieSettings);
registerChartBlock(doughnutSettings);
registerChartBlock(lineSettings);
registerChartBlock(horizontalBarSettings);
registerChartBlock(horizontalLineSettings);
registerChartBlock(polarAreaSettings);
registerChartBlock(radarSettings);
registerChartBlock(scatterSettings);
