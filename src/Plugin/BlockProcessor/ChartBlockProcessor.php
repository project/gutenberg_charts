<?php

namespace Drupal\gutenberg_charts\Plugin\BlockProcessor;

use Drupal\gutenberg\BlockProcessor\GutenbergBlockProcessorInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\Core\Cache\CacheableMetadata;

class ChartBlockProcessor implements GutenbergBlockProcessorInterface {

  public function isSupported(array $block, $block_content = '') {
    $blockName = $block['blockName'] ?? '';
    $supportedBlocks = [
      'rm-gutenberg-landingpage-builder/bar',
      'rm-gutenberg-landingpage-builder/pie',
      'rm-gutenberg-landingpage-builder/doughnut',
      'rm-gutenberg-landingpage-builder/line',
      'rm-gutenberg-landingpage-builder/horizontal-bar',
      'rm-gutenberg-landingpage-builder/horizontal-line',
      'rm-gutenberg-landingpage-builder/scatter',
      'rm-gutenberg-landingpage-builder/radar',
      'rm-gutenberg-landingpage-builder/polar-area',
    ];

    return in_array($blockName, $supportedBlocks);
  }

  public function processBlock(array &$block, &$block_content, RefinableCacheableDependencyInterface $bubbleable_metadata) {
    if ($this->isSupported($block, $block_content)) {
      $renderer = \Drupal::service('renderer');
      $build = [
        '#type' => 'markup',
        '#markup' => Markup::create($block_content),
        '#attached' => [
          'library' => [
            'gutenberg_charts/chartjs',
          ],
        ],
      ];

      $block_content = $renderer->render($build);

      $cache_metadata = CacheableMetadata::createFromRenderArray($build);
      $bubbleable_metadata->addCacheableDependency($cache_metadata);
    }
  }
}
