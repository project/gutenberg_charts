const config = require("@wordpress/scripts/config/webpack.config");

module.exports = {
	...config,
	entry: {
		...config.entry,
		"gutenberg-editor": "./styles/gutenberg-editor.scss",
		frontend: "./styles/frontend.scss",
		global: "./blocks/global.scss",
		blocks: "./blocks/index.js",
	},
	output: {
		...config.output,
		path: `${__dirname}/dist`,
		filename: "[name].js",
	},
};
